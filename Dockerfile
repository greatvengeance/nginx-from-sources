FROM debian:9 AS clearnginx
RUN apt-get update && apt-get install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.20.0.tar.gz
RUN tar xvfz nginx-1.20.0.tar.gz && cd nginx-1.20.0 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=clearnginx /usr/local/nginx/conf/ /usr/local/nginx/conf/
COPY --from=clearnginx /usr/local/nginx/html/ /usr/local/nginx/html/
COPY --from=clearnginx /usr/local/nginx/logs/ /usr/local/nginx/logs/
COPY --from=clearnginx /usr/local/nginx/sbin/nginx .
COPY nginx.conf /usr/local/nginx/conf/nginx.conf
RUN touch /usr/local/nginx/logs/access.log
RUN touch /usr/local/nginx/logs/error.log
RUN chmod +x /usr/local/nginx/sbin/nginx 
CMD ["./nginx", "-g", "daemon off;"]
